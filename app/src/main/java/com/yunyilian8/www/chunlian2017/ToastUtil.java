package com.yunyilian8.www.chunlian2017;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

public class ToastUtil {
    public static Toast mToast = null;
    public static int Short_Show = Toast.LENGTH_SHORT;
    public static int Long_Show = Toast.LENGTH_LONG;

    /**
     * 提示
     *
     * @param activity
     * @param msg
     * @param time     0=short 1=long
     * @param gravity
     */
    public static void showToast(Context activity, String msg, int time, int gravity, int marginTop, int marginBottom) {
        if (null != msg && !msg.equals("")) {
            if (mToast == null)
                mToast = Toast.makeText(activity, msg, time);
            else {
                mToast.setText(msg);
                mToast.setDuration(time);
            }
            mToast.setGravity(gravity, marginTop, marginBottom);
            mToast.show();
        }
    }

    /**
     * 中间提示
     * @param context
     * @param msg
     */
    public static void showToast(Context context, String msg) {
        showToast(context, msg, Short_Show, Gravity.CENTER, 0, 0);
    }

    /**
     * 中间提示
     * @param context
     * @param msgResId
     *         资源字符串Id
     */
    public static void showToast(Context context, int msgResId) {
        showToast(context, context.getString(msgResId), Short_Show, Gravity.CENTER, 0, 0);
    }

    public static void cancelToast() {
        if (mToast != null)
            mToast.cancel();
        mToast = null;
    }
}
