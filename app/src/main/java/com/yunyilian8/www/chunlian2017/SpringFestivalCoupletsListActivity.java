package com.yunyilian8.www.chunlian2017;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.widget.ListView;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/1/20.
 * 春联列表
 */

public class SpringFestivalCoupletsListActivity extends Activity {
    private int mType;
    private ListView mListView;
    File mFile;
    private List<String> mList = new ArrayList<>();
    private TitleBarLayout mTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spring_list);

        //获取当前对联类型
        initType();
        //初始化view
        initView();
        //初始化数据
        initData();
    }

    private void initData() {
        //从assets文件夹中读取txt文件数据
        readData();
        if (null != mList)
        {
            MyAdapter adapter = new MyAdapter(this,mList);
            mListView.setAdapter(adapter);
        }
    }

    /**
     * 根据type类型读取数据
     */
    private void readData() {
        if (mType < 0)
            return;
        switch (mType){
                case 1:
                    //5联
                    mTitle.setTitleText("五字春联");
                    readFile("text01.txt");
                    break;
                case 2:
                    //7联
                    mTitle.setTitleText("七字春联");
                    readFile("text02.txt");
                    break;
                case 3:
                    //9联
                    mTitle.setTitleText("九字春联");
                    readFile("text03.txt");
                    break;
                case 4:
                    //11联
                    mTitle.setTitleText("十一字春联");
                    readFile("text04.txt");
                    break;
                case 5:
                    //11联以上
                    mTitle.setTitleText("十一字以上春联");
                    readFile("text05.txt");
                    break;

        }
    }

    /**
     * 读取txt文件的对联信息
     * @param fileName
     */
    private void readFile(String fileName) {
        InputStream open = null;
        try {
            AssetManager manager = getAssets();
            open =  manager.open(fileName);
            BufferedReader dataIO = new BufferedReader(new InputStreamReader(open,"UTF-8"));
            String strLine = null;
            while((strLine =  dataIO.readLine()) != null) {
                mList.add(strLine);
            }
            dataIO.close();
            open.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //初始化view
    private void initView() {
        mListView = (ListView) findViewById(R.id.ll_data);
        mTitle = (TitleBarLayout)findViewById(R.id.titlebar_layout);
    }

    //获取类型
    private void initType() {
        Intent intent = getIntent();
        mType = intent.getIntExtra("type", -1);
    }
}
