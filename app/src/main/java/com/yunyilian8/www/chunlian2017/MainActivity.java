package com.yunyilian8.www.chunlian2017;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button mBtn01;
    private Button mBtn02;
    private Button mBtn03;
    private Button mBtn04;
    private Button mBtn05;
    private boolean mIsMenuOpened = false;
    private long mExitTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
        setListener();
    }

    private void setListener() {
        mBtn01.setOnClickListener(this);
        mBtn02.setOnClickListener(this);
        mBtn03.setOnClickListener(this);
        mBtn04.setOnClickListener(this);
        mBtn05.setOnClickListener(this);
    }

    private void initView() {
        mBtn01 = (Button) findViewById(R.id.btn_01);
        mBtn02 = (Button) findViewById(R.id.btn_02);
        mBtn03 = (Button) findViewById(R.id.btn_03);
        mBtn04 = (Button) findViewById(R.id.btn_04);
        mBtn05 = (Button) findViewById(R.id.btn_05);
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId())
        {
            case R.id.btn_01:
                //5联
                intent = new Intent(this,SpringFestivalCoupletsListActivity.class);
                intent.putExtra("type",1);
                startActivity(intent);
                break;
            case R.id.btn_02:
                //7联
                intent = new Intent(this,SpringFestivalCoupletsListActivity.class);
                intent.putExtra("type",2);
                startActivity(intent);
                break;
            case R.id.btn_03:
                //9联
                intent = new Intent(this,SpringFestivalCoupletsListActivity.class);
                intent.putExtra("type",3);
                startActivity(intent);
                break;
            case R.id.btn_04:
                //11联
                intent = new Intent(this,SpringFestivalCoupletsListActivity.class);
                intent.putExtra("type",4);
                startActivity(intent);
                break;
            case R.id.btn_05:
                //11联以上
                intent = new Intent(this,SpringFestivalCoupletsListActivity.class);
                intent.putExtra("type",5);
                startActivity(intent);
                break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (!mIsMenuOpened) {
                if ((System.currentTimeMillis() - mExitTime) > 2000) {
                    ToastUtil.showToast(getApplication(), "再按一次退出程序");
                    mExitTime = System.currentTimeMillis();
                } else {
                    finish();
                }
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
