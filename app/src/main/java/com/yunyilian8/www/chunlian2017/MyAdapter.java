package com.yunyilian8.www.chunlian2017;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.List;

/**
 * Created by Administrator on 2017/1/20.
 */

public class MyAdapter extends BaseAdapter {

    private Context mContext;
    private List<String> mList;

    public MyAdapter(Context context, List<String> list) {
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        MyHolder holder = null;
        if (convertView == null) {
            holder = new MyHolder();
            convertView = View.inflate(mContext, R.layout.item, null);
            holder.tv01 = (TextView) convertView.findViewById(R.id.tv01);
            holder.tv02 = (TextView) convertView.findViewById(R.id.tv02);
            holder.tvNum = (TextView) convertView.findViewById(R.id.tv_num);

            convertView.setTag(holder);
        } else {
            holder = (MyHolder) convertView.getTag();
        }

        holder.tv01.setText(mList.get(position).split(",")[0]);
        holder.tv02.setText(mList.get(position).split(",")[1]);
        holder.tvNum.setText(position + 1 + "");

        return convertView;
    }

    class MyHolder {
        TextView tv01;
        TextView tv02;
        TextView tvNum;
    }
}
