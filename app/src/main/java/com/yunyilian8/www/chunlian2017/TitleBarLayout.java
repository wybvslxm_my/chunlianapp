package com.yunyilian8.www.chunlian2017;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


/**
 * Created by Kermit on 2015/8/21.
 */
public class TitleBarLayout extends RelativeLayout {

    private static final String TAG = "TitleBarLayout";
    private static final int DEFAULT_BANK_ICON = R.mipmap.icon_back;
    private static final int DEFAULT_TEXT_COLOR = Color.WHITE;
    protected static final int DEFAULT_BACKRESOURCE = R.drawable.ka_action_click_selector;

    protected boolean showHome = true;
    protected boolean homeBackClick = false;
    protected boolean showTitle = true;
    protected boolean showAction = false;
    protected boolean showActionImage = false;
    protected int textColor  = getResources().getColor(R.color.white);
    protected float mActionTextSize;
    protected String mTitle;
    protected String mActionText;

    private int iconHome = DEFAULT_BANK_ICON;
    private int iconActionImage = R.mipmap.icon_back;
    private int backgroundColor = R.color.yellow1;

    private static final int TITLE_BACK_ID = 0x1000;
    private static final int ACTION_ID = 0x1001;
    private static final int ACTION_IMAGE_ID = 0x1002;

    protected RelativeLayout mBackLayout;
    private RelativeLayout mTitleLayout;
    private RelativeLayout mActionLayout;
    private RelativeLayout mActionImageLayout;

    private TextView mTitleTextView;
    private TextView action;
    private ImageView actionImage;

    private TitleBarListener mListener;

    ViewClickListener viewClickListener = new ViewClickListener();
    private int mWindowWidth = 0;

    public TitleBarLayout(Context context) {
        super(context);
        initType(context, null, 0, 0);
        initView();
    }

    public TitleBarLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initType(context, attrs, 0, 0);
        initView();
    }

    public TitleBarLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initType(context, attrs, defStyleAttr, 0);
        initView();
    }

    @SuppressLint("21")
    public TitleBarLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleRes);
        initType(context, attrs, defStyleAttr, defStyleRes);
        initView();
    }

    protected void initType(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.TitleBarLayout, defStyleAttr, defStyleRes);

        int n = a.getIndexCount();
        for (int i = 0; i < n; i++) {
            int attr = a.getIndex(i);
            switch (attr) {
                case R.styleable.TitleBarLayout_showHome:
                    showHome = a.getBoolean(attr, true);
                    break;
                case R.styleable.TitleBarLayout_showAction:
                    showAction = a.getBoolean(attr, false);
                    break;
                case R.styleable.TitleBarLayout_showActionImage:
                    showActionImage = a.getBoolean(attr, false);
                    break;
                case R.styleable.TitleBarLayout_iconHome:
                    iconHome = a.getResourceId(attr, DEFAULT_BANK_ICON);
                    break;
                case R.styleable.TitleBarLayout_iconActionImage:
                    iconActionImage = a.getResourceId(attr, DEFAULT_BANK_ICON);
                    break;
                case R.styleable.TitleBarLayout_backcolor:
                    backgroundColor = a.getResourceId(attr, R.color.white );
                    break;
                case R.styleable.TitleBarLayout_midtitle:
                    mTitle = a.getString(attr);
                    break;
                case R.styleable.TitleBarLayout_actionText:
                    mActionText = a.getString(attr);
                    break;
                case R.styleable.TitleBarLayout_homeBackClick:
                    homeBackClick = a.getBoolean(attr, false);
                    break;
                case R.styleable.TitleBarLayout_titleTextColor01:
                    textColor = a.getColor(attr,getResources().getColor(R.color.color_F1F4F4));
                    break;
                case R.styleable.TitleBarLayout_actionTextSize:
                    mActionTextSize = a.getDimension(attr,4);
                    break;
            }
        }
        a.recycle();
    }

    protected void initView() {
        setGravity(Gravity.CENTER_VERTICAL);
        if (showHome) {
            addView(addHomeBackView());
        }
        if (showActionImage) {
            addView(addActionImageView());
        }
        if (showAction) {
            addView(addActionView());
        }
        if (showTitle) {
            addView(addTitleView());
        }
        setBackgroundResource(backgroundColor);

    }

    private View addHomeBackView() {
        mBackLayout = new RelativeLayout(getContext());
        LayoutParams lp = new LayoutParams(UITools.dip2px(getContext(), 44), LayoutParams.MATCH_PARENT);
        lp.addRule(ALIGN_PARENT_LEFT, 1);
        mBackLayout.setId(TITLE_BACK_ID);
        mBackLayout.setBackgroundResource(DEFAULT_BACKRESOURCE);
        mBackLayout.setLayoutParams(lp);
        mBackLayout.setOnClickListener(viewClickListener);
        ImageView back = new ImageView(getContext());
        RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        param.addRule(RelativeLayout.CENTER_IN_PARENT);
        back.setLayoutParams(param);
        back.setImageResource(iconHome);
        mBackLayout.addView(back);
        // mBackLayout.setVisibility(GONE);
        return mBackLayout;
    }

    private View addActionView() {
        mActionLayout = new RelativeLayout(getContext());
        LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
        if (null == mActionImageLayout) {
            lp.addRule(ALIGN_PARENT_RIGHT);
        } else {
            lp.addRule(LEFT_OF, mActionImageLayout.getId());
        }
        mActionLayout.setId(ACTION_ID);
        mActionLayout.setBackgroundResource(DEFAULT_BACKRESOURCE);
        mActionLayout.setLayoutParams(lp);
        if (showAction) {
            mActionLayout.setVisibility(VISIBLE);
        } else {
            mActionLayout.setVisibility(GONE);
        }
        mActionLayout.setOnClickListener(viewClickListener);
        action = new TextView(getContext());
        RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        param.addRule(RelativeLayout.CENTER_IN_PARENT);
        action.setLayoutParams(param);
        action.setPadding(UITools.dip2px(getContext(), 10), 0, UITools.dip2px(getContext(), 10), 0);
        action.setSingleLine();
        if (mActionTextSize == 0)
            mActionTextSize = 14;
        action.setTextSize(mActionTextSize);
        action.setGravity(CENTER_IN_PARENT);
        action.setTextColor(textColor);
        if (!TextUtils.isEmpty(mActionText)) {
            action.setText(mActionText);
        }
        mActionLayout.addView(action);

        return mActionLayout;
    }

    private View addActionImageView() {
        mActionImageLayout = new RelativeLayout(getContext());
        LayoutParams lp = new LayoutParams(UITools.dip2px(getContext(), 44), LayoutParams.MATCH_PARENT);
        lp.addRule(ALIGN_PARENT_RIGHT);
        mActionImageLayout.setId(ACTION_IMAGE_ID);
        mActionImageLayout.setBackgroundResource(DEFAULT_BACKRESOURCE);
        mActionImageLayout.setLayoutParams(lp);
        if (showActionImage) {
            mActionImageLayout.setVisibility(VISIBLE);
        } else {
            mActionImageLayout.setVisibility(GONE);
        }
        mActionImageLayout.setOnClickListener(viewClickListener);
        actionImage = new ImageView(getContext());
        RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        param.addRule(RelativeLayout.CENTER_IN_PARENT);
        actionImage.setLayoutParams(param);
        actionImage.setImageResource(iconActionImage);
        mActionImageLayout.addView(actionImage);
        return mActionImageLayout;
    }

    private View addTitleView() {
        mTitleLayout = new RelativeLayout(getContext());
        if (null == mActionImageLayout || mActionImageLayout.getVisibility() == GONE) {
            resetTitleBarTitleParams(false);
        } else {
            resetTitleBarTitleParams();
        }
        mTitleTextView = new TextView(getContext());
        RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        param.addRule(RelativeLayout.CENTER_IN_PARENT);
        mTitleTextView.setLayoutParams(param);
        mTitleTextView.setMaxWidth(UITools.dip2px(getContext(), 220));
        mTitleTextView.setTextSize(18);
        mTitleTextView.setMarqueeRepeatLimit(-1);
        mTitleTextView.setGravity(CENTER_HORIZONTAL);
        mTitleTextView.setTextColor(textColor);
        mTitleTextView.setText(mTitle);
        mTitleLayout.addView(mTitleTextView);

        return mTitleLayout;
    }

    public void setTitleText(String text) {
        if (null != mTitleTextView) {
            mTitleTextView.setText(text);
        }
    }

    public void setTitleText(int resId) {
        if (null != mTitleTextView) {
            mTitleTextView.setText(getContext().getString(resId));
        }
    }

    public void showActionButton(boolean showAction) {
        if (null != mActionLayout) {
            if (showAction) {
                mActionLayout.setVisibility(View.VISIBLE);
            } else {
                mActionLayout.setVisibility(View.GONE);
            }
        }
    }

    public void showActionImageButton(boolean showActionImage){
        if (null != mActionImageLayout) {
            if (showActionImage) {
                mActionImageLayout.setVisibility(View.VISIBLE);
            } else {
                mActionImageLayout.setVisibility(View.GONE);
            }
        }
    }

    /**
     * 右上角文字按钮设置
     */
    public void setActionText(String text) {
        if (showAction && null != action) {
            action.setText(text);
        }
    }

    /**
     * 右上角文字按钮设置
     */
    public void setActionText(int res) {
        if (showAction && null != action) {
            action.setText(getContext().getString(res));
        }
    }

    /**
     * 右上角图片按钮
     */
    public void setActionImage(int res) {
        if (showAction && null != action) {
            actionImage.setImageResource(iconActionImage);
        }
    }


    private class ViewClickListener implements OnClickListener {
        @Override
        public void onClick(View v) {
            if (v == mBackLayout) {
                if (!homeBackClick) {
                    if (mListener != null) {
                        mListener.onBackClick();
                    }
                } else {
                    ((Activity) getContext()).onBackPressed();
                    ((Activity) getContext()).finish();
                }
            } else if (v == mActionLayout) {
                if (mListener != null) {
                    mListener.onActionClick();
                }
            } else if (v == mActionImageLayout) {
                if (mListener != null) {
                    mListener.onActionImageClick();
                }
            }

        }
    }

    public void setTitleBarListener(TitleBarListener l) {
        mListener = l;
    }

    //显示返回按钮
    public void showTitleBarHomeIcon() {
        if (null != mBackLayout) {
            mBackLayout.setVisibility(VISIBLE);
        }
    }

    //隐藏返回按钮
    public void hideTitleBarHomeIcon(boolean b) {
        if (null != mBackLayout) {
            mBackLayout.setVisibility(GONE);
        }
    }

    /**
     * 关闭按钮是否显示时，调整Title的宽度
     */
    private void resetTitleBarTitleParams() {
        LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        int margin = 0;
        if ((null == mActionImageLayout || mActionImageLayout.getVisibility() == GONE) && (null == mActionLayout || mActionLayout.getVisibility() == GONE)) {
            margin = 44;
        } else {
            margin = 88;
        }
        lp.setMargins(UITools.dip2px(getContext(), margin), 0, UITools.dip2px(getContext(), margin), 0);
        mTitleLayout.setLayoutParams(lp);
    }

    /**
     * 强制设置Title的宽度，当返回键不显示时，左边最小的间隔为44dp
     *
     * @param b
     */
    private void resetTitleBarTitleParams(boolean b) {
        LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        int margin = b ? 88 : 44;
        lp.setMargins(UITools.dip2px(getContext(), margin), 0, UITools.dip2px(getContext(), margin), 0);
        mTitleLayout.setLayoutParams(lp);
    }

    public interface TitleBarListener {
        void onBackClick();

        void onActionImageClick();

        void onActionClick();

    }

}
